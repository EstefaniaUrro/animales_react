const ANIMALES = {
    Aguila: 'https://www.anipedia.net/imagenes/reproduccion-del-aguila-imperial-oriental.jpg',
    Ardilla: 'https://www.anipedia.net/imagenes/ardilla-sobre-arbol.jpg',
    Ballena: 'https://www.anipedia.net/imagenes/ballenas-2.jpg',
    Caballo: 'https://www.anipedia.net/imagenes/caracteristicas-generales-de-los-caballos.jpg',
    Cocodrilo: 'https://www.anipedia.net/imagenes/cocodrilos-fotos.jpg',
    Delfin: 'https://www.anipedia.net/imagenes/delfines-800x375.jpg'

}
export default ANIMALES