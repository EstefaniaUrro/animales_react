import React, { Component } from 'react';
import PropTypes from 'prop-types';

const ANIMALES = {
    Aguila: 'https://www.anipedia.net/imagenes/reproduccion-del-aguila-imperial-oriental.jpg',
    Ardilla: 'https://www.anipedia.net/imagenes/ardilla-sobre-arbol.jpg',
    Ballena: 'https://www.anipedia.net/imagenes/ballenas-2.jpg',
    Caballo: 'https://www.anipedia.net/imagenes/caracteristicas-generales-de-los-caballos.jpg',
    Cocodrilo: 'https://www.anipedia.net/imagenes/cocodrilos-fotos.jpg',
    Delfin: 'https://www.anipedia.net/imagenes/delfines-800x375.jpg'
}
export default class ImagenAnimal extends Component {
    static propTypes = {
        animal: PropTypes.oneOf(['Aguila', 'Ardilla', 'Ballena', 'Caballo', 'Cocodrilo', 'Delfin'])
    }
    static defaultProps = {
        animal: 'Aguila'
    }

    state = { src: ANIMALES[this.props.animal] }
    render() {
        return (
            <div>
                <p>Esto es un {this.props.animal}</p>
                <img
                    alt={this.props.animal}
                    src={this.state.src}
                    width='250'
                />
            </div >
        )
    }
}