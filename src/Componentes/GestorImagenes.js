import React, { Component } from 'react'
import ImagenAnimal1 from './ImagenAnimal1'
import ANIMALES from './animales.js';

export default class GestorImagenes extends Component {
    state = { animal: 'Aguila' }
    render() {
        const lista = Object.keys(ANIMALES);

        return (
            <div>
                <br />
                {lista.map(nombreAnimal => (
                    <button
                        className="btn btn-info"
                        key={nombreAnimal}
                        onClick={() => this.setState({ animal: nombreAnimal })}
                        disabled={nombreAnimal === this.state.animal}
                    >
                        {nombreAnimal}
                    </button>
                ))}
                <br />
                <ImagenAnimal1 animal={this.state.animal} />
            </div>
        )
    }
}
