import React, { Component } from 'react'
import PropTypes from 'prop-types';

import ANIMALES from './animales.js';

export default class ImagenAnimal01 extends Component {
    static propTypes = {
        animal: PropTypes.oneOf(Object.keys(ANIMALES))//declara que es una de las que aparece en la lista
    }
    static defaultProps = {
        animal: 'Aguila'
    }

    state = { src: ANIMALES[this.props.animal] }

    componentWillReceiveProps(nextProps) {//se ele modifica las props y recoge las nuevas props para actualizar el state
        this.setState({ src: ANIMALES[nextProps.animal] })
    }

    componentDidUpdate(prevProps, prevState) {//renderiza 
        const img = document.querySelector('img');
        img.animate([
            {
                filter: 'blur(2px)'
            },
            {
                filter: 'blur(opx)'
            }
        ], {
            duration: 1250,
            easing: 'ease'
        });
    }

    render() {

        return (
            <div>
                <h2>Visor de animales</h2>

                <p>Esto es un {this.props.animal}</p>
                <img
                    alt={this.props.animal}
                    src={this.state.src}
                    width='550'
                />
            </div>
        )
    }
}


