import React from 'react';
import './App.css';
import GestorImagenes from './Componentes/GestorImagenes';
import '../src/Componentes/estilos.css';

function App() {
  return (
    <div className="App">
      <GestorImagenes />
    </div>
  );
}

export default App;
